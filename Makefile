all: clean build flash

clean:
	sudo rm -rf .pioenvs

build:
	sudo pio run

flash:
	sudo pio run -t upload
	
monitor:
	sudo pio device monitor
