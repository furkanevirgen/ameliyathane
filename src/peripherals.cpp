#include "main.hpp"

void openValve(bool valve) {
  if (valve == water) {
    digitalWrite(waterValvePin, LOW);
  } else if (valve == soap) {
    digitalWrite(soapValvePin, LOW);
  }
}

void closeValve(bool valve) {
  if (valve == water) {
    digitalWrite(waterValvePin, HIGH);
  } else if (valve == soap) {
    digitalWrite(soapValvePin, HIGH);
  }
}

void checkSoap() {
  if (digitalRead(soapSensorPin) == 0) {
    openValve(soap);
    delay(1000);
    closeValve(soap);
  }
}

void checkWater(void) {
  if (digitalRead(waterSensorPin) == 0) {
    openValve(water);
     uint8_t min = EEPROM.read(addrmin);
     uint8_t sec = EEPROM.read(addrsec);
    countdown(min,sec);
    closeValve(water);
  }
}

byte button(void) {
  int button = analogRead(buttonPin);

  if (button > 1000)
    return none;

  if (button < 20)
    return right;

  if (button < 120 && button > 50)
    return up;

  if (button < 300 && button > 150)
    return down;

  if (button < 450 && button > 320)
    return left;

  if (button < 650 && button > 500)
    return set;
}

// byte buttonstate(void) {
//   int rightcounter = 0, leftcounter = 0, upcounter = 0, downcounter = 0,
//       setcounter = 0;
//   int laststate = none;
//   int buttonstate = button();
//
//   if (buttonstate != laststate) {
//     switch (buttonstate) {
//
//     case right:
//       rightcounter++;
//       break;
//
//     case left:
//       leftcounter++;
//       break;
//
//     case up:
//       upcounter++;
//       break;
//
//     case down:
//       downcounter++;
//       break;
//
//     case set:
//       setcounter++;
//       break;
//     }
//   }
// }
