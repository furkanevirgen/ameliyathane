#include "main.hpp"

void sleepMode(void) {
  delay(1000);
  lcd.clear();
  digitalWrite(lcdLight, LOW);
}

void printTimer(uint8_t tim) {
  int min=tim/60;
  int sec=tim%60;
  digitalWrite(lcdLight,HIGH);
  lcd.setCursor(3, 0);
  lcd.print("Kalan Sure");
  lcd.setCursor(7, 1);
  lcd.print(":");

  if (min > 9) {
    lcd.setCursor(5, 1);
    lcd.print(min);
  } else if (min < 10) {
    lcd.setCursor(5, 1);
    lcd.print("0");
    lcd.setCursor(6, 1);
    lcd.print(min);
  }
  if (sec > 9) {
    lcd.setCursor(8, 1);
    lcd.print(sec);
  } else if (sec < 10) {
    lcd.setCursor(8, 1);
    lcd.print("0");
    lcd.setCursor(9, 1);
    lcd.print(sec);
  }
}

void countdown(uint8_t min, uint8_t sec) {
  unsigned long now,tim=sec;
  sec=min*60+sec;
  while (tim > 0) {
    checkSoap();
    if (millis()/1000 - now > 0){
    now=millis()/1000;
    tim=sec--;
    printTimer(tim);
        }
     }
  }
