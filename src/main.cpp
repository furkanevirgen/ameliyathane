#include "main.hpp"

LiquidCrystal lcd(8, 9, 4, 5, 6, 7);

void setup() {
  lcd.begin(16, 2);
  pinMode(waterValvePin, OUTPUT);
  digitalWrite(waterValvePin, HIGH);
  pinMode(soapValvePin, OUTPUT);
  digitalWrite(soapValvePin, HIGH);
  pinMode(soapSensorPin, INPUT);
  pinMode(waterSensorPin, INPUT);
  pinMode(lcdLight, OUTPUT);
  Serial.begin(9600);
}

 void loop() {
   checkWater();
   checkSoap();
   settingsMode();
   sleepMode();
 }


// void loop() {
//   byte buton = button();
//   Serial.println(buton);
//   delay(1000);
// }




// void loop() {
//   // digitalWrite(lcdLight, HIGH);
//   // setSecond(5);
// }
//


// void eepromtest(void){
//   int address0 = 3;
//   int address1 = 4;
//   int data0 = 10;
//   int data1 = 20;
//   int value0 = 0;
//   int value1 = 0;
//
//   EEPROM.write(address0, data0);
//   EEPROM.write(address1, data1);
//
//   value0 = EEPROM.read(address0);
//   value1 = EEPROM.read(address1);
//
//   digitalWrite(lcdLight, HIGH);
//   lcd.setCursor(0,0);
//   lcd.print(value0);
//   lcd.setCursor(0,1);
//   lcd.print(value1);
// }




// void loop() {
//   digitalWrite(lcdLight, HIGH);
//   //  setSecond(5);
//   timerSetting();
// }
//
// void loop() {
//   unsigned int now, anan=10;
//   Serial.begin(9600);
//   now = millis()/1000;
//   Serial.println(anan-now);
//   delay(100);
// }
