#include "main.hpp"

void setMinute(uint8_t min) {
  lcd.setCursor(0, 0);
  lcd.print("     Dakika    >");

  if (min > 9) {
    lcd.setCursor(7, 1);
    lcd.print(min);
  } else {
    lcd.setCursor(7, 1);
    lcd.print("0");
    lcd.setCursor(8, 1);
    lcd.print(min);
  }
}

void setSecond(uint8_t sec) {
  lcd.setCursor(0, 0);
  lcd.print("<    Saniye     ");

  if (sec > 9) {
    lcd.setCursor(7, 1);
    lcd.print(sec);
  } else {
    lcd.setCursor(7, 1);
    lcd.print("0");
    lcd.setCursor(8, 1);
    lcd.print(sec);
  }
}

void timerSetting(void) {
  uint8_t min = EEPROM.read(addrmin);
  uint8_t sec = EEPROM.read(addrsec);
  byte state = 2;
  while(true){
  switch (state){

    case 0:
    while (true) {
      lcd.setCursor(0, 0);
      lcd.print("      Sure      ");
      lcd.setCursor(0, 1);
      lcd.print("   Ayarlandi    ");
      EEPROM.write(addrmin, min);
      EEPROM.write(addrsec, sec);
      delay(1000);
      asm volatile ("  jmp 0");
    }

    case 1:
    while (true){
      setSecond(sec);
      if (button()==up){
        if (sec < 59){
          sec++;
          setSecond(sec);
          delay(200);
        }
      }
      else if (button()==down){
        if (sec > 0){
          sec--;
          setSecond(sec);
          delay(200);
        }
      }
      else if(button()==left){state=2;break;}
      else if(button()==set){state=0;break;}
    }

    case 2:
      while (true){
      setMinute(min);
        if (button()==up){
          min++;
          setMinute(min);
          delay(200);
        }
        else if (button()==down){
          if (min > 0){
          min--;
          setMinute(min);
          delay(200);
          }
        }
        else if(button()==right){state=1;break;}
        else if(button()==set){state=0;break;}
      }
    }
  }
}
void settingsMode(void) {
  if (button() == set) {
    delay(300);
    digitalWrite(lcdLight, HIGH);
    timerSetting();
  }
}
