#include <Arduino.h>
#include <LiquidCrystal.h>
#include <EEPROM.h>


#define buttonPin A0
#define soapSensorPin A3
#define waterSensorPin A5
#define soapValvePin 2
#define waterValvePin 3
#define lcdLight 10

#define soap 0
#define water 1

#define addrmin 0
#define addrsec 1

#define none 0
#define set 1
#define left 2
#define right 3
#define up 4
#define down 5

extern LiquidCrystal lcd;

void sleepMode(void);
void openValve(bool);
void closeValve(bool);
void checkWater(void);
void checkSoap(void);
byte button(void);
// byte buttonstate(void);
void printTimer(uint8_t sec);
void countdown(uint8_t min, uint8_t sec);
void settingsMode(void);
void timerSetting(void);
void setMinute(uint8_t);
void setSecond(uint8_t);
